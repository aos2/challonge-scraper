#!/usr/bin/env python3

from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup, Comment, CData
import re
import os
import sys


def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return
    text content, otherwise return None.
    """
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error('Error during requests to {0} : {1}'.format(url, str(e)))
        return None


def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200
            and content_type is not None
            and content_type.find('html') > -1)


def log_error(e):
    print(e)


def get_bracket(markup):
    bracket = markup.find_all('script')[6].get_text()
    return bracket


def get_standings(markup):
    standings = markup.find_all('table', class_='standings')[0].find_all('tr')
    return standings


def get_tournament_type(markup):
    tournament_type = re.findall(
        r'"tournament_type":"([^"]*)"', markup.get_text())[0]
    return tournament_type


def parse_standings(markup, output, *args, top=4, no_topcut=True):
    standings = get_standings(markup)
    table = ('<table class="max-width text-center"><thead><tr>'
             '<th>Player</th>'
             '<th>W-L-T</th>'
             '<th>Tiebreakers</th>'
             '</tr></thead></table>'
             )
    table = BeautifulSoup(table, 'html.parser')
    winner = None
    for i in range(1, 1+top):
        s = standings[i]
        player = re.findall(
            r'(?:"/>\n([^"]+)\n[^<]+)<a[^>]+>([^"]+)</a>', str(s))
        player = player[0] if not player == [] else re.findall(
            r'<a[^>]+>([^"]+)</a>', str(s))
        if winner is None:
            winner = player
        wlt = s.find_all('td')[2].get_text()
        tie = s.find_all('td')[5].get_text()
        row = f'<tr><td>{player[0]}</td><td>{wlt}</td><td>{tie}</td></tr>'
        row = BeautifulSoup(row, 'html.parser')
        table.table.append(row)

    output.write('{{% details Top ' + str(top) + ' %}}\n')
    output.write(table.prettify())
    if no_topcut:
        output.write(f'\n\nCongratulations to **{winner[0]}**!\n')
    output.write('{{% /details %}}')


def parse_bracket(markup, tournament_type, output):
    bracket = get_bracket(markup)
    players = list(reversed(re.findall(r'"display_name":"([^"]*)"', bracket)))
    rounds = [int(i) for i in list(reversed(re.findall(
        r'"round":(-?[0-9]*),', bracket)))]
    scores = list(reversed(re.findall(r'"scores":([^"]*),', bracket)))
    finals = max(rounds) if not tournament_type == "swiss" else False
    lfinals = min(rounds)

    while rounds:
        rd = rounds.pop()
        count = rounds.count(rd)
        rounds = rounds[:len(rounds)-count]
        if rd == finals:
            p1, p2 = players.pop(), players.pop()
            output.write('{{% details Grand Finals Pt1 %}}\n')
            score = scores.pop()[1:-1].split(',')
            output.write(('{{< matchup' + f' "{p1}" "{p2}" '
                          f'{score[0]}-{score[1]} ' + '>}}\n'))
            output.write('{{% /details %}}\n\n')
            output.write('{{% details Grand Finals Pt2 %}}\n')
            if count:
                p1, p2 = players.pop(), players.pop()
                score = scores.pop()[1:-1].split(',')
                output.write(('{{< matchup '
                              f'"{p1}" "{p2}" '
                              f'{score[0]}-{score[1]} ' + '>}}\n'))
            output.write((f'Congratulations to '
                          f'**{p1 if score[0] > score[1] else p2}**!\n'))
        else:
            pre = (
                ("Winners " if rd >= 0 else "Losers ")
                if tournament_type == "double elimination" else "")
            if rd == finals - 1 > 0 or rd == lfinals < 0:
                post = "Finals"
            elif rd == finals - 2 > 0 or rd == lfinals + 1 < 0:
                post = "Semis"
            elif rd == finals - 3 > 0 or rd == lfinals + 2 < 0:
                post = "Quarters"
            elif rd == lfinals + 3 < 0:
                post = "Top 8"
            elif rd == 0:
                post = "Bronze Match"
            else:
                post = "Round " + str(abs(rd))
            output.write('{{% details' + f' {pre}{post} ' + '%}}\n')
            for _ in range(count+1):
                score = scores.pop()
                score = ['0', '0'] if score == '[]' else score[1:-1].split(',')
                output.write(('{{< matchup '
                              f'"{players.pop()}" "{players.pop()}" '
                              f'{score[0]}-{score[1]}' + ' >}}\n'))
        output.write('{{% /details %}}')
        if rounds:
            output.write('\n\n')


def main(tourney_url):
    parsed = BeautifulSoup(simple_get(tourney_url), 'html.parser')
    path = f"{os.getcwd()}/{tourney_url.split('/')[-1]}.md"
    if os.path.isfile(path):
        open(path, 'w').close()  # empty
    else:
        os.mknod(path)  # touch
    output_file = open(path, 'w')
    tournament_type = get_tournament_type(parsed)
    parse_bracket(parsed, tournament_type, output_file)
    if tournament_type == "swiss":
        output_file.write('\n\n')
        parse_standings(parsed, output_file, top=4)


def parse_topcut(swiss_url, topcut_url, *_, top=8):
    path = f"{os.getcwd()}/{swiss_url.split('/')[-1]}.md"
    if os.path.isfile(path):
        open(path, 'w').close()  # empty
    else:
        os.mknod(path)  # touch
    output_file = open(path, 'w')

    swiss = BeautifulSoup(simple_get(swiss_url), 'html.parser')
    output_file.write('# Swiss\n')
    output_file.write('## Rounds\n')
    parse_bracket(swiss, "swiss", output_file)
    output_file.write('\n\n')
    output_file.write('## Results\n')
    parse_standings(swiss, output_file, top, no_topcut=False)

    topcut = BeautifulSoup(simple_get(topcut_url), 'html.parser')
    output_file.write('\n\n')
    output_file.write('# Top Cut\n')
    tournament_type = get_tournament_type(topcut)
    parse_bracket(topcut, tournament_type, output_file)


if __name__ == "__main__":
    main(sys.argv[1])
