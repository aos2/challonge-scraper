## challonge.py

Python script that pulls from [Challonge] metadata and writes the matchups and
results by round in a markup consumable by [our Aos2 tracker][tracker]. Code to make
the initial web requests is from [this guide][guide].

The main idea is that all the metadata Challonge uses to generate the bracket is commented
inside of a `<script>` tag and we merely use regular expression groups to capture our
desired information. This scraper should be easily adaptable for other purposes.

## requirements

* [beautifulsoup4] *available on [pip]*
* [python3.7]

## usage

```sh
python3 challonge.py "https://challonge.com/accelweekly10" # Swiss
./challonge.py https://twt2018kr.challonge.com/t16 # Double Elimination

# Two part format: Top 8 of Swiss seeded into Single Elimination
python3
import challonge
challonge.parse_topcut("https://challonge.com/accelweekly15",
    "https://challonge.com/Accel15top8", top=8)
```

[beautifulsoup4]: https://www.crummy.com/software/BeautifulSoup/
[Challonge]: https://challonge.com/
[guide]: https://realpython.com/python-web-scraping-practical-introduction/
[pip]: https://pypi.org/project/beautifulsoup4/
[python3.7]: https://www.python.org/downloads/release/python-370/
[tracker]: https://gitlab.com/aos2/aos2.gitlab.io
